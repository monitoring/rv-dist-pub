---
RVDist: Runtime Verification of Distributed Component-Based Systems
---

## About ##

RVDist is a prototype tool for LTL runtime verification of distributed (component-based) systems, written in the C++ programming language.

RVDist uses [Spot](https://spot.lrde.epita.fr/) platform which is a C++11 library for LTL manipulation, and [BigInt](http://www.qlambda.com/2012/01/compile-and-install-bigint-as-shared.html) library which allows us to do arithmetic on big size integers.

## How To Run RVDist

Requirements:

* C++11
* Spot library
* BigInt library

To run the RVDist tool, please follow these steps:
1. Download the RVDist tool-set in [.tar.gz](https://gitlab.inria.fr/monitoring/rv-dist-pub/blob/master/dist/RVDist.tar.gz) format or [.zip](https://gitlab.inria.fr/monitoring/rv-dist-pub/blob/master/dist/RVDist.zip) format.
2. Extract the package. The main directory contains:
    * **Make** directory which contains:
        main C++ codes and associated files,
        a sample list of events **event.data**,
        a sample configuration file **config.ini**.
    * **examples/** directory that contains some examples in separate folders.
3. To use the tool on command line, go to the **Make** directory and run the following commands:

`$ g++ -std=c++11 -lspot -lbigint -o main`

`$ ./main`

A virtual machine with all dependencies installed is available [here](https://drive.google.com/file/d/0B8IkvONfBevAeDl5RUFfaFQ0T1k/view?usp=sharing). (Username: user, Password: z).
